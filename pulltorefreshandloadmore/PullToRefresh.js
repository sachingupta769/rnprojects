import React from 'react';

import {View, Text, FlatList, SafeAreaView} from 'react-native';

class PullToRefresh extends React.Component {
  constructor() {
    super();
    this.state = {
      refreshing: false,
      data: [
        {name: 'sachin'},
        {name: 'Pavan'},
        {name: 'Anvita'},
        {name: 'Mona'},
      ],
    };
  }
  handleOnRefresh = () => {
    this.setState({refreshing: true}, () => {
      this.setState({
        data: [...this.state.data, {name: 'Sunny'}],
        refreshing: false,
      });
    });
  };
  render() {
    const {refreshing} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        <FlatList
          data={this.state.data}
          renderItem={({item, index}) => {
            return <Text>{item.name}</Text>;
          }}
          onRefresh={() => this.handleOnRefresh()}
          refreshing={refreshing}
        />
      </SafeAreaView>
    );
  }
}

export default PullToRefresh;
